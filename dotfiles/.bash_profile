# This is read by X on startup

if [ -x $HOME/.bashrc ]; then
    . $HOME/.bashrc
fi

SSH_ENV="$HOME/.ssh/environment"

function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add;
}

# Source SSH settings, if applicable
if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    ps ${SSH_AGENT_PID} | grep ssh-agent > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi

if [ "$SSH_AUTH_SOCK" != "" ]; then
    for KEY in mbligh@mongodb id_rsa mbligh-AWS.pem aws.pem
    do
        if [ -f $HOME/.ssh/$KEY ]; then
            ssh-add $HOME/.ssh/$KEY 2>&1 | grep -v 'Identity added:'
        fi
    done
fi

# /usr/bin/dropbox start

/usr/bin/setxkbmap -option 'ctrl:nocaps'
