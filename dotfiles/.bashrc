# This is run for every xterm

export PATH="$HOME/bin:/home/mbligh/anaconda3/bin:$PATH"
export PYTHONPATH="$HOME/bin:$PYTHONPATH"
export LOCALE=C
export LC_ALL=C
export LANG=C
export EDITOR=vi
unset HISTFILE
unset MAILCHECK
unset MAILPATH

export AWS1="mongodb@ec2-52-10-86-174.us-west-2.compute.amazonaws.com"
export AWS2="mongodb@ec2-52-25-233-135.us-west-2.compute.amazonaws.com"
export AWS3="mongodb@ec2-52-25-233-137.us-west-2.compute.amazonaws.com"

alias diff='diff -u'
stty -echoctl
set -o vi
alias ls='ls -h'
alias i=get_iplayer
alias youtube-dl="youtube-dl --restrict-filenames --prefer-ffmpeg"

export NVM_DIR="/home/mbligh/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
