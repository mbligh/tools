---
- hosts: localhost
  become: yes
  tasks:
  - name: REPO chrome key
    apt_key:
      url: https://dl.google.com/linux/linux_signing_key.pub
  - name: REPO google-chrome
    apt_repository:
      repo: deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main
      state: present
      filename: google-chrome
  - name: REPO mongodb key
    apt_key:
      url: https://www.mongodb.org/static/pgp/server-4.2.asc
  - name: REPO mongodb
    apt_repository:
      repo: deb [arch=amd64] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse
      state: present
      filename: mongodb
  - name: REPO get-iplayer
    apt_repository:
      repo: ppa:jon-hedgerows/get-iplayer
      filename: get-iplayer
      codename: bionic
  - name: REPO handbrake
    apt_repository:
      repo: ppa:stebbins/handbrake-releases
      filename: handbrake
      codename: bionic
  - name: Remove apt packages
    apt:
      name: [ anacron, mono-runtime, mono-runtime-common, mono-runtime-sgen, avahi-autoipd,
              avahi-daemon, thunderbird, transmission-gtk, transmission-common, totem,
              totem-common, libtotem-plparser18, unclutter ]
  - name: Update and upgrade
    apt:
      autoremove: yes
      upgrade: yes
      update_cache: yes
  - name: Install apt packages
    apt:
      name: [ asciidoc, audacity, autoconf, automake, binutils-dev, bison, build-essential,
              build-essential, chromium-browser, clang, clang-format, console-terminus, coreutils,
              curl, deluge, dia, dnsutils, dropbox, easytag, evince, ffmpeg, flashplugin-installer,
              flex, fonts-inconsolata, g++, gcc, gdisk, geany, geeqie, get-iplayer, gimp, git,
              golang, google-chrome-stable, graphviz, handbrake, htop, ipython, ipython3,
              jupyter-notebook, lame, less, libarchive13, libaudit-dev, libavcodec-dev, libbz2-1.0,
              libbz2-dev, libc6-dbg, libc6-dev, libcurl4-openssl-dev, libdw-dev, libelf-dev,
              libev-dev, libexpat1-dev, libgl1-mesa-dev, libgssapi-krb5-2, libgtk2.0-dev,
              libiberty-dev, libkrb5-dev, liblzma-dev, libnuma-dev, libpcap-dev, libperl-dev,
              libqt4-dev, libslang2-dev, libsnmp-dev, libssl-dev, libssl-dev, libstdc++6-4.8-dbg,
              libtool, libunwind8, libunwind8-dev, lightdm, linux-cloud-tools-generic,
              linux-tools-common, linux-tools-generic, mailutils, mdadm, mediainfo, mysql-client,
              nasm, nfs-common, numactl, openssh-server, patch, pigz, pkg-config, ppa-purge, python,
              python-audit, python-bs4-doc, python-mysqldb, python-pandas-doc, python-pip,
              python-setuptools, python-webpy,
              python3, python3-numexpr, python3-pandas, python3-pip, python3-scipy,
              python3-setuptools, python3-tables, qt4-qtconfig, rsync, rtmpdump, strace, sysstat,
              tcpdump, telnet, time, transcode, tshark, ttf-dejavu, ttf-ubuntu-font-family,
              ttf-xfree86-nonfree, ubuntu-restricted-extras, vim, vlc, whois, wireshark,
              x11-xkb-utils, xauth, xclip, xfonts-terminus, xfonts-traditional, xfsprogs,
              xfswitch-plugin, xmlto, xpra, xscreensaver, xscreensaver-data,
              xscreensaver-data-extra, xscreensaver-gl, xscreensaver-extra, xterm, xvfb, 
              youtube-dl, zstd ]
  - name: PIA openvpn dir
    file:
      path: /tmp/openvpn
      state: directory
      mode: '0755'
  - name: PIA openvpn files
    unarchive:
      src: https://www.privateinternetaccess.com/openvpn/openvpn.zip
      dest: /tmp/openvpn
      remote_src: yes
  - name: remove utter crap from motd
    file: path=/etc/update-motd.d/[1-9]* state=absent
  - pip:
      name: [ wheel, youtube_dl, blist, python-amazon-mws, cprofilev, sortedcontainers,
              m3-cdecimal, pymongo, parsedatetime, ply==3.4, slimit ]
  - sysctl: name=net.ipv6.conf.all.disable_ipv6 value=1
  - sysctl: name=net.ipv6.conf.default.disable_ipv6 value=1
  - sysctl: name=net.ipv6.conf.lo.disable_ipv6 value=1
  - sysctl: name=kernel.kptr_restrict value=0
  - sysctl: name=kernel.yama.ptrace_scope value=0
  - sysctl: name=vm.dirty_ratio value=75
  - name: mbligh sudo nopasswd
    lineinfile:
      path: /etc/sudoers.d/mbligh
      create: yes
      line: 'mbligh ALL=(ALL) NOPASSWD: ALL'
